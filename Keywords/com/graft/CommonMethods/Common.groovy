package com.graft.CommonMethods;


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class Common {

	/*   Function : getDataDictionary (def TestCaseName, def ExcelFilePath)
	 Author   : Venkatesh.Balagani
	 Inputs   : TestCaseName  : Test case name
	 ExcelFilePath : Path of Excel File
	 Return   : Returns DICTIONARY.
	 @Usage   : def excelDataDictionary OR list of dataDictionaries[if multiples matches found]
	 excelDataDictionary = CustomKeywords.'com.excel.getExcelData.getDataDictionary'('TC_01', "Unicon_TestData/justForTestSample") for [Single match]
	 String uID          = excelDataDictionary['UID']
	 String password     = excelDataDictionary['PASSWORD']
	 @for multiple match of test cases
	 int testCaseMatchCount = CustomKeywords.'com.unicon.utils.ReadDataFromExcel.getTestCaseMatchCount'('TC_DifferentDataToRun', 'Data Files/Unicon_TestData/AAD_TestData')
	 def listOfDictionaries = CustomKeywords.'com.unicon.utils.ReadDataFromExcel.getDataDictionary'('TC_DifferentDataToRun', 'Data Files/Unicon_TestData/AAD_TestData')
	 println listOfDictionaries
	 //@Usage: for a testCase having different data TC: TC_DifferentDataToRun
	 for (int rowIndex : ( 0 .. ( testCaseMatchCount - 1 ) ) ) {
	 //def dictionaryList  = CustomKeywords.'com.unicon.utils.ReadDataFromExcel.getDataDictionary'('TC_DifferentDataToRun', 'Data Files/Unicon_TestData/AAD_TestData')
	 def dictionary = [:]
	 dictionary = listOfDictionaries[rowIndex]
	 int testCaseNumber = rowIndex + 1
	 println "TestCase Number: " + testCaseNumber + " - UserName: " + dictionary['UserName']
	 println "TestCase Number: " + testCaseNumber + " - Password: " + dictionary['Password']
	 dictionary.empty
	 }
	 //@Usage: for a testCase having different data TC: TC_1
	 def dictionaryTwo = CustomKeywords.'com.unicon.utils.ReadDataFromExcel.getDataDictionary'('TC_1', 'Data Files/Unicon_TestData/AAD_TestData')
	 println dictionaryTwo['TIN']
	 */

	@Keyword
	def getDataDictionary (def TestCaseName, def ExcelFilePath) {

		/*  getAllData()    : It will fetch all the rows with data as list.
		 Example> [[TC_NAME, UID, PASSWORD],[TC_01, Practice.Admin, Ksf1wdZSJRB4PlH4AIJWTQ==]]
		 getRowNumbers()    : Returns NUMBER OF ROWS with DATA
		 getColumnNumbers() : Returns NUMBER OF COLUMNS with DATA
		 */

		def allData            = []
		allData                = findTestData(ExcelFilePath).getAllData()
		int rowNumber          = findTestData(ExcelFilePath).getRowNumbers()
		int columnNumber       = findTestData(ExcelFilePath).getColumnNumbers()
		int count              = 0
		def listOfDictionaries = []

		for (def rowIndex : (0 .. (rowNumber - 1))) {
			if ( allData[rowIndex][0] == TestCaseName ) {
				count = count + 1
			} else {
				// doing nothing
			}
		}
		println "Testcase match count: " + count

		def match = 0
		for (def rowIndex : (0 .. (rowNumber - 1))) {

			def dataDictionary = [:]
			if ( allData[rowIndex][0] == TestCaseName ) {
				match = match + 1
				//Defining Map [ Groovy - Maps. A Map (also known as an associative array, dictionary, table and hash) ]
				for (def columnIndex : (0 .. (columnNumber - 1))) {
					//def dataDictionary = [:]
					//Writing the Column Name and respective values as KEY:VALUE pair into DICTIONARY : dataDictionary
					dataDictionary.put(allData[0][columnIndex], allData[rowIndex][columnIndex])
				}
				//if (count == 1 ) {
				//Writing the Column Name and respective values as KEY:VALUE pair into DICTIONARY : dataDictionary
				//return dataDictionary
				//} else {
				listOfDictionaries.add(dataDictionary)
				dataDictionary.empty
				if ( match == count ) {
					return listOfDictionaries
				}
				//}

			}
		}
	}

	//This function returns test case match count from excel sheet
	@Keyword
	def getTestCaseMatchCount (def testCaseName, def excelFilePath) {
		def allData          = []
		allData              = findTestData(excelFilePath).getAllData()
		int rowNumber        = findTestData(excelFilePath).getRowNumbers()
		int columnNumber     = findTestData(excelFilePath).getColumnNumbers()
		int count            = 0
		for (def rowIndex : (0 .. (rowNumber - 1))) {
			if ( allData[rowIndex][0] == testCaseName ) {
				count = count + 1
			} else {
				// doing nothing
			}
		}
		return count
	}

}




