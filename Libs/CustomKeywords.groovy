
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */


def static "com.graft.CommonMethods.Common.getDataDictionary"(
    	Object TestCaseName	
     , 	Object ExcelFilePath	) {
    (new com.graft.CommonMethods.Common()).getDataDictionary(
        	TestCaseName
         , 	ExcelFilePath)
}

def static "com.graft.CommonMethods.Common.getTestCaseMatchCount"(
    	Object testCaseName	
     , 	Object excelFilePath	) {
    (new com.graft.CommonMethods.Common()).getTestCaseMatchCount(
        	testCaseName
         , 	excelFilePath)
}
