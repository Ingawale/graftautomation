import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\nilesh.ingawale\\Desktop\\Release\\Graft\\GRAFT.apk', true)

//@Usage: for a testCase having different data TC: TC_DifferentDataToRun
for (int rowIndex : (0..testCaseMatchCount - 1)) {
	//def dictionaryList  = CustomKeywords.'com.unicon.utils.ReadDataFromExcel.getDataDictionary'('TC_DifferentDataToRun', 'Data Files/Unicon_TestData/AAD_TestData')
	def dictionary = [:]

	dictionary = (listOfDictionaries[rowIndex])

	int testCaseNumber = rowIndex + 1

	println(('Row Number: ' + rowIndex) + 1)

	Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

	Mobile.setText(findTestObject('Login/txtFld_EnterUserName'), dictionary['UserName'], 0, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

	Mobile.setText(findTestObject('Login/txtFld_EnterPassword'), dictionary['Password'], 0, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.tap(findTestObject('Login/btn_Login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

	if (testCaseNumber == 1) {
		Mobile.verifyElementText(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/valMsg_WrongPasswordAttempt1'),
			dictionary['Validation messages'])

		Mobile.tap(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/btn_OK'), 0)
	} else if (testCaseNumber == 2) {
		Mobile.verifyElementText(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/valMsg_WrongPasswordAttempt2'),
			dictionary['Validation messages'])

		Mobile.tap(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/btn_OK'), 0)
	} else if (testCaseNumber == 3) {
		Mobile.verifyElementText(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/valMsg_WrongPasswordAttempt3'),
			dictionary['Validation messages'])

		Mobile.tap(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/btn_OK'), 0)
	} else if (testCaseNumber == 4) {
		Mobile.verifyElementText(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/valMsg_WrongPasswordAttempt4'),
			dictionary['Validation messages'])

		Mobile.tap(findTestObject('Login/AlertWhenUserEntersIncorrectPassword/btn_OK'), 0)
	}
}