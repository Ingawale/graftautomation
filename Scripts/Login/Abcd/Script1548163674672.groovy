import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

int testCaseMatchCount = CustomKeywords.'com.graft.CommonMethods.Common.getTestCaseMatchCount'('VerifyUserIsAbleToLoginWithValidCredentials',
	'Data Files/DataSheetLogin')

def listOfDictionaries = CustomKeywords.'com.graft.CommonMethods.Common.getDataDictionary'('VerifyUserIsAbleToLoginWithValidCredentials',
	'Data Files/DataSheetLogin')

//@Usage: for a testCase having different data TC: TC_DifferentDataToRun
for (int rowIndex : (0..testCaseMatchCount - 1)) {
	//def dictionaryList  = CustomKeywords.'com.unicon.utils.ReadDataFromExcel.getDataDictionary'('TC_DifferentDataToRun', 'Data Files/Unicon_TestData/AAD_TestData')
	def dictionary = [:]

	dictionary = (listOfDictionaries[rowIndex])

	int testCaseNumber = rowIndex + 1

	Mobile.startApplication('C:\\Users\\nilesh.ingawale\\Desktop\\Release\\Graft\\GRAFT.apk', true)

	Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

	Mobile.setText(findTestObject('Login/txtFld_EnterUserName'), dictionary['UserName'], 0, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

	Mobile.setText(findTestObject('Login/txtFld_EnterPassword'), dictionary['Password'], 0, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.tap(findTestObject('Login/btn_Login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

	Mobile.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

	boolean AlertButtonOk = Mobile.verifyElementExist(findTestObject('Login/btn_OK_AppIsAlreadyRegistered'), 5, FailureHandling.CONTINUE_ON_FAILURE)

	println('ok button status ' + AlertButtonOk)

	if (AlertButtonOk == true)
	{
		Mobile.tap(findTestObject('Login/btn_OK_AppIsAlreadyRegistered'), 0)

		//  Mobile.setText(findTestObject('SetPin/txtFld_SetPin'), '123456', 0)
		//    Mobile.tap(findTestObject('Login/btn_OK_AppIsAlreadyRegistered'), 0)
		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
		
	}
	else
	{
		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

	}
}
